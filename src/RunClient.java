import javax.swing.*;

/**
 * Justin Brooks
 * CS350 Project 6
 *
 * Main method for client instance
 */

public class RunClient {

    static Client client1;

    public static void main(String[] args) {
        JFrame clientApp = new JFrame("Dominoes Player 2");
        if (args.length >= 1) {
            client1 = new Client(args[0]);
        }
        else {
            client1 = new Client( "127.0.0.1" );
        }
        clientApp.add(client1);
        clientApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        clientApp.setSize(600,600);
        clientApp.setVisible(true);
        client1.runClient();
    }
}
