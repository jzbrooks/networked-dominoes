import java.awt.*;
import java.io.Serializable;

/**
 * Justin Brooks
 * CS 350
 * Project 6
 *
 * Class representing a domino. Handles location, drawing, rotating of the domino.
 *
 */
public class CDomino implements Serializable {

    // Numbers drawn on domino initially
    private int topNumber;
    private int bottomNumber;

    // Location of the four points that represent the corners of the domino
    private int[] Ys = new int[4];
    private int[] Xs = new int[4];

    // Represent the height and width of the domino
    private final int width = 35;
    private final int height = 70;

    // Tracks center x coordinate and center y coordinate
    private int centerX;
    private int centerY;

    // Dot location for the domino and radius
    private int dotX;
    private int dotY;
    private final int dotRadius = 4;

    // Keeps track of the number of rotations for a domino
    private int isRotated = 0;

    // Draws dots if true
    private boolean isDecorated = true;

    // Three dimensional matrix representing the grid of dots
    // and the patterns associated with numbers
    private final int numberGrid[][][] = {
            {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}, // 0 dots
            {{0, 0, 0}, {0, 1, 0}, {0, 0, 0}}, // 1 dot
            {{1, 0, 0}, {0, 0, 0}, {0, 0, 1}}, // 2 dots
            {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}, // 3 dots
            {{1, 0, 1}, {0, 0, 0}, {1, 0, 1}}, // 4 dots
            {{1, 0, 1}, {0, 1, 0}, {1, 0, 1}}, // 5 dots
            {{1, 0, 1}, {1, 0, 1}, {1, 0, 1}}  // 6 dots
    };

    // Constructor sets up initial location and numbers on domino
    public CDomino(int locationX, int locationY,
                   int topNum, int bottomNum) {
        topNumber = topNum;
        bottomNumber = bottomNum;
        Xs[0] = locationX;
        Ys[0] = locationY;
        Xs[1] = locationX + width;
        Ys[1] = locationY;
        Xs[2] = locationX + width;
        Ys[2] = locationY + height;
        Xs[3] = locationX;
        Ys[3] = locationY + height;
        centerX = locationX + (width/2);
        centerY = locationY + (height/2);
    }

    // Draws domino and associated components in all orientations
    public void drawDomino(Graphics graphics, boolean player1, boolean upsideDown) {
        graphics.setColor(Color.lightGray);
        graphics.fillPolygon(this.Xs, this.Ys, 4);
        graphics.setColor(Color.blue);
        graphics.drawPolygon(this.Xs, this.Ys, 4);
        if (isDecorated) {
            drawDominoDecorations(graphics);
        }
    }

    // Sets drawing dots
    public void setDecorated(boolean dec) {
        isDecorated = dec;
    }

    public void drawDominoDecorations(Graphics graphics) {
        graphics.setColor(Color.red);
        if (isRotated%4 == 1)
            graphics.drawLine(this.Xs[0] - (height/2), this.Ys[0], this.Xs[0]-(height/2), this.Ys[2]);
        else if (isRotated%4 == 2)
            graphics.drawLine(this.Xs[0], this.Ys[1] - height/2, this.Xs[1], this.Ys[1] - height/2);
        else if (isRotated%4 == 3)
            graphics.drawLine(this.Xs[0] + (height/2), this.Ys[0], this.Xs[0] + (height/2), this.Ys[2]);
        else
            graphics.drawLine(this.Xs[0], this.centerY, this.Xs[1], this.centerY);

        // Dots for initial orientation
        if (isRotated%4==0) {
            dotX = Xs[0] + 4;
            dotY = Ys[0] + 5;
            for (int i=0;i<3;i++) {
                for (int j=0;j<3;j++) {
                    if (numberGrid[topNumber][i][j] == 1) {
                        graphics.fillOval(dotX, dotY, dotRadius*2, dotRadius*2);
                    }
                    dotX += 10;
                }
                dotX = Xs[0] + 4;
                dotY += 10;
            }

            dotX = Xs[0] + 4;
            dotY = this.centerY + 5;
            for (int i=0;i<3;i++) {
                for (int j=0;j<3;j++) {
                    if (numberGrid[bottomNumber][i][j] == 1) {
                        graphics.fillOval(dotX, dotY, dotRadius*2, dotRadius*2);
                    }
                    dotX += 10;
                }
                dotX = Xs[0] + 4;
                dotY += 10;
            }
        }

        // Dots for rotation 1 (90 degrees clockwise)
        if (isRotated%4==1) {
            dotX = Xs[0] - 13;
            dotY = Ys[0] + 5;
            for (int i=0;i<3;i++) {
                for (int j=0;j<3;j++) {
                    if (numberGrid[topNumber][i][j] == 1) {
                        graphics.fillOval(dotX, dotY, dotRadius*2, dotRadius*2);
                    }
                    dotY += 10;
                }
                dotY = Ys[0] + 5;
                dotX -= 10;
            }

            dotX = Xs[0] - (height/2) - 13;
            dotY = Ys[0] + 4;
            for (int i=0;i<3;i++) {
                for (int j=0;j<3;j++) {
                    if (numberGrid[bottomNumber][i][j] == 1) {
                        graphics.fillOval(dotX, dotY, dotRadius*2, dotRadius*2);
                    }
                    dotY += 10;
                }
                dotY = Ys[0] + 4;
                dotX -= 10;
            }
        }

        // Dots for rotation 2 (180 clockwise from initial)
        if (isRotated%4==2) {
            dotX = Xs[2] + 4;
            dotY = Ys[2] + 5;
            for (int i=0;i<3;i++) {
                for (int j=0;j<3;j++) {
                    if (numberGrid[bottomNumber][i][j] == 1) {
                        graphics.fillOval(dotX, dotY, dotRadius*2, dotRadius*2);
                    }
                    dotX += 10;
                }
                dotX = Xs[2] + 4;
                dotY += 10;
            }

            dotX = Xs[2] + 4;
            dotY = this.centerY + 5;
            for (int i=0;i<3;i++) {
                for (int j=0;j<3;j++) {
                    if (numberGrid[topNumber][i][j] == 1) {
                        graphics.fillOval(dotX, dotY, dotRadius*2, dotRadius*2);
                    }
                    dotX += 10;
                }
                dotX = Xs[2] + 4;
                dotY += 10;
            }
        }

        // Rotation 3 (270 degrees from initial orientation)
        if (isRotated%4==3) {
            dotX = Xs[2] - 13;
            dotY = Ys[1] + 4;
            for (int i=0;i<3;i++) {
                for (int j=0;j<3;j++) {
                    if (numberGrid[bottomNumber][i][j] == 1) {
                        graphics.fillOval(dotX, dotY, dotRadius*2, dotRadius*2);
                    }
                    dotY += 10;
                }
                dotY = Ys[1] + 4;
                dotX -= 10;
            }

            dotX = Xs[0] + 4;
            dotY = Ys[0] - 13;
            for (int i=0;i<3;i++) {
                for (int j=0;j<3;j++) {
                    if (numberGrid[topNumber][i][j] == 1) {
                        graphics.fillOval(dotX, dotY, dotRadius*2, dotRadius*2);
                    }
                    dotY -= 10;
                }
                dotY = Ys[0] -13;
                dotX += 10;
            }
        }
    }

    // Handles moving domino
    public void translate(int dx, int dy) {
        for (int i=0; i<4; i++) {
            Xs[i] += dx;
            Ys[i] += dy;
        }

        if (isRotated%4==0) {
            centerX = Xs[0] + (width/2);
            centerY = Ys[0] + (height/2);
        }
        else if (isRotated%4==1) {
            centerX = Xs[3] + (height/2);
            centerY = Ys[3] + (width/2);
        }
        else if (isRotated%4==2) {
            centerX = Xs[2] + (width/2);
            centerY = Ys[2] + (height/2);
        }
        else if (isRotated%4==3) {
            centerX = Xs[1] + (height/2);
            centerY = Ys[1] + (width/2);
        }
    }

    // Returns true if (x,y) is inside domino
    public boolean isInside(int x, int y) {
        if (isRotated%4==0) {
            if (x <= this.Xs[1] && x >= this.Xs[0] && y <= this.Ys[2] && y >= this.Ys[0]) {
                return true;
            }
        }
        else if (isRotated%4==1) {
            if (x <= this.Xs[0] && x >= this.Xs[3] && y <= this.Ys[2] && y >= this.Ys[3] )
                return true;
        }
        else if (isRotated%4==2) {
            if (x <= this.Xs[3] && x >= this.Xs[2] && y <= this.Ys[1] && y >= this.Ys[2])
                return true;
        }
        else if (isRotated%4==3) {
            if (x <= this.Xs[2] && x >= this.Xs[0] && y <= this.Ys[0] && y >= this.Ys[1])
                return true;
        }

        return false;
    }

    // Handles rotation of a domino
    public void rotate (int deg) {

        double theta = Math.PI/180*deg;	// in radians
        for (int i=0; i<4; i++) {
            double tmp_x;
            tmp_x = centerX + (Xs[i] - centerX) * Math.cos(theta) - (Ys[i] - centerY) * Math.sin(theta);
            this.Ys[i] = (int) (centerY + (Xs[i] - centerX) * Math.sin(theta) + (Ys[i] - centerY) * Math.cos(theta) + 0.5);
            this.Xs[i] = (int) tmp_x;
        }
        isRotated++;
    }

    // Mutator for location of the domino
    public void setLocation(int x, int y) {
        Xs[0] = x;
        Ys[0] = y;
        Xs[1] = x + width;
        Ys[1] = y;
        Xs[2] = x + width;
        Ys[2] = y + height;
        Xs[3] = x;
        Ys[3] = y + height;
        centerX = x + (width/2);
        centerY = y + (height/2);
    }

    // Overloaded mutator
    public void setLocation(int[] x, int[] y) {
        Xs[0] = x[0];
        Ys[0] = y[0];
        Xs[1] = x[1];
        Ys[1] = y[1];
        Xs[2] = x[2];
        Ys[2] = y[2];
        Xs[3] = x[3];
        Ys[3] = y[3];
        if (isRotated%4==0) {
            centerX = Xs[0] + (width/2);
            centerY = Ys[0] + (height/2);
        }
        else if (isRotated%4==1) {
            centerX = Xs[3] + (height/2);
            centerY = Ys[3] + (width/2);
        }
        else if (isRotated%4==2) {
            centerX = Xs[2] + (width/2);
            centerY = Ys[2] + (height/2);
        }
        else if (isRotated%4==3) {
            centerX = Xs[1] + (height/2);
            centerY = Ys[1] + (width/2);
        }
    }

    public int[] getXs() {
        return this.Xs;
    }

    public int[] getYs() {
        return this.Ys;
    }

    public int getCenterX() { return this.centerX; }

    public int getCenterY() { return this.centerY; }
    // prints {topNumber, bottomNumber}
    @Override
    public String toString() {
        String s = "";
        s+= "{";
        s+= topNumber;
        s+= ", ";
        s+= bottomNumber;
        s+= "}";
        return s;
    }
}
