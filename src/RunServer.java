import javax.swing.*;

/**
 * Justin Brooks
 * CS350 Project 6
 *
 * Main method for a server instance
 */
public class RunServer {

    public static void main(String[] args) {
        JFrame serverApp = new JFrame("Dominoes Player 1");
        Server server1 = new Server();
        serverApp.add(server1);
        serverApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        serverApp.setSize(600, 600);
        serverApp.setVisible(true);
        server1.runServer();
    }
}
