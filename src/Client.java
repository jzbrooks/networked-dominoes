/**
 * Justin Brooks
 * CS350 Project 6
 *
 * Client side window for player two of the networked domino game.
 * Moves are processed by sending messages to the client based
 * on mouse events in this window.
 */

import javax.swing.*;
import java.awt.event.*;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.awt.*;
import java.util.ArrayList;

public class Client extends JPanel implements MouseMotionListener, MouseListener, KeyListener
{
    private ObjectOutputStream output; // output stream to server
    private ObjectInputStream input; // input stream from server
    private String chatServer; // host server for this application
    private Socket client; // socket to communicate with server

    // Domino game members
    private ArrayList<CDomino> player1; // server-side player
    private ArrayList<CDomino> player2; // client-side player

    private Integer moveIndex;

    // Used for moving the domino on mouse event
    private CDomino pieceToBeMoved = null;
    private int oldX;
    private int oldY;

    // State Presentation
    public void paint( Graphics g )
    {
        // State Presentation, using double buffers
        // create the back buffer
        Image backBuffer=createImage(getSize().width, getSize().height);
        Graphics gBackBuffer=backBuffer.getGraphics();
        // clear the back buffer
        gBackBuffer.setColor(Color.white);
        gBackBuffer.clearRect(0, 0, getSize().width, getSize().height);
        gBackBuffer.setColor(Color.blue);
        gBackBuffer.drawLine(0,105,getWidth(),105);
        gBackBuffer.drawLine(0,getHeight()-105,getWidth(),getHeight()-105);
        // draw the pieces to back buffer

        if (player1!=null) {
            for (CDomino dom : player1) {
                dom.drawDomino(gBackBuffer, false, false);
            }
        }
        if (player2!=null) {
            for (CDomino dom : player2) {
                dom.drawDomino(gBackBuffer, false, false);
            }
        }
        // copy from back buffer to front
        g.drawImage(backBuffer, 0, 0, null);
    } // end method paintComponent

    // MouseListener event handlers
    // If right click -> Rotate 90 degrees clockwise
    public void mouseClicked( MouseEvent e )
    {
        String message = "ROTATE ";
        if (e.isMetaDown()) {
            for (int i=0; i<player2.size(); i++) {
                CDomino piece = player2.get(i);
                if (piece.isInside(e.getX(), e.getY())) {
                    piece.rotate(90);
                    message += i;
                    try {
                        output.writeObject(message);
                    } catch (IOException ex) {
                        System.out.println("MouseClicked message not sent.");
                    }
                    repaint();
                    break;
                }
            }
        }
    }

    // Prepares domino to be moved when clicked
    public void mousePressed( MouseEvent e )
    {
        String message = "SELECTED ";
        // search from top down (i.e. back to front)
        for (int i=player2.size()-1; i>=0; i--) {
            CDomino piece=player2.get(i);
            if (piece.isInside(e.getX(), e.getY())) {
                pieceToBeMoved=piece;
                oldX=e.getX();
                oldY=e.getY();
                message += String.format("%d %d %d", i, oldX, oldY);
                try {
                    output.writeObject(message);
                } catch (IOException ex) {
                    System.out.println("MousePressed message not sent.");
                }
                repaint();
                break;
            }
        }
    }

    // set piece to be moved to null when mouse released
    public void mouseReleased( MouseEvent e )
    {
        pieceToBeMoved=null; // no shape selected
    }

    public void mouseEntered( MouseEvent e )
    {
    }

    public void mouseExited( MouseEvent e )
    {
    }

    // MouseMotionListener event handlers
    public void mouseMoved( MouseEvent e )
    {
    }

    // Move domino with mouse dragged
    public void mouseDragged( MouseEvent e )
    {
        if (e.isMetaDown()) return;	// ignore right button

        String message = "MOVE ";
        if (pieceToBeMoved!=null) {
            int newX = e.getX();
            int newY = e.getY();
            pieceToBeMoved.translate(e.getX()-oldX, e.getY()-oldY);
            oldX=e.getX();
            oldY=e.getY();
            message += String.format("%4d %4d %4d %4d", newX, newY, oldX, oldY);
            try {
                output.writeObject(message);
            } catch (IOException ex) {
                System.out.println("MouseDragged message not sent.");
            }
            repaint();
        }
    }

    // Restart the game when r is pressed
    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_R) {
            JOptionPane opt = new JOptionPane();
            opt.showMessageDialog(this, "Please ask player 1 to restart the game.");
            opt.setVisible( true );
        }
    }

    public void keyReleased(KeyEvent e)
    {
    }

    public void keyTyped(KeyEvent e)
    {
    }


    /*
    *
    * Networking methods for Domino Client
    *
    */


    // initialize chatServer and set up GUI
    public Client( String host )
    {
        chatServer = host; // set server to which this client connects

        this.setFocusable( true );
        this.addMouseListener( this );
        this.addKeyListener( this );
        this.addMouseMotionListener( this );
    }

    public void runClient()
    {
        try
        {
            connectToServer();
            getStreams();
            processConnection();
        }
        catch ( EOFException eofException )
        {
        }
        catch ( IOException ioException )
        {
            ioException.printStackTrace();
        }
        finally
        {
            closeConnection();
        }
    }

    private void connectToServer() throws IOException
    {
        // create Socket to make connection to server
        client = new Socket( InetAddress.getByName( chatServer ), 12345 );

        JOptionPane opt = new JOptionPane();
        opt.showMessageDialog(this, "Connection successful.");
        opt.setVisible( true );

    }

    private void getStreams() throws IOException
    {
        // set up output stream for objects
        output = new ObjectOutputStream( client.getOutputStream() );

        // set up input stream for objects
        input = new ObjectInputStream( client.getInputStream() );

        catchInit();
        repaint();
    }

    private void processConnection() throws IOException
    {
        // Recieves messages from client and manipulates components accordingly
        while (client.isConnected()) {
            String message = null;

            try {
                message = (String) input.readObject();
            } catch (ClassNotFoundException ex) {
                System.out.println("Message not read in client.");
            }


            if (message.contains("SELECTED")) {
                String tuple = message.substring(9,message.length());
                String[] oldCoordinates = tuple.split("\\s+");
                moveIndex = Integer.parseInt(oldCoordinates[0]);
                pieceToBeMoved = player1.get(moveIndex);
                pieceToBeMoved.setDecorated(true);
                oldX = Integer.parseInt(oldCoordinates[1]);
                oldY = Integer.parseInt(oldCoordinates[2]);
            }

            if (message.contains("ROTATE")) {
                String tuple = message.substring(7,message.length());
                int rotateIndex = Integer.parseInt(tuple);
                CDomino piece = player1.get(rotateIndex);
                piece.rotate(90);
                repaint();
            }

            if (message.contains("MOVE")) {
                String tuple = message.substring(6,message.length()).trim();
                String[] coordinates = tuple.split("\\s+");
                try {
                    int newX = Integer.parseInt(coordinates[0]);
                    int newY = Integer.parseInt(coordinates[1]);
                    pieceToBeMoved.translate(newX-oldX, oldY-newY);
                    oldX = Integer.parseInt(coordinates[2]);
                    oldY = Integer.parseInt(coordinates[3]);
                } catch (NumberFormatException ex) {
                    System.out.println("Message From Server: BAD FORMAT");
                }
                repaint();
            }

            if (message.contains("RESTART")) {
                player1 = player2 = null;
                catchInit();
                repaint();
            }
        }
    }

    private void closeConnection()
    {
        try
        {
            output.close();
            input.close();
            client.close();
        }
        catch ( IOException ioException )
        {
            ioException.printStackTrace();
        }
    }

    // Catches the initialization data from server
    private void catchInit() {
        try {
            player1 = ( ArrayList<CDomino> ) input.readObject();
            player2 = ( ArrayList<CDomino> ) input.readObject();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch ( IOException ex ) {
            ex.printStackTrace();
        }

        int locationX1 = 85;
        for (CDomino dom : player1) {
            dom.setDecorated(false);
            dom.setLocation(locationX1, 13);
            locationX1 += 65;
        }

        locationX1 = 85;
        for (CDomino dom : player2) {
            dom.setDecorated(true);
            dom.setLocation(locationX1, 475);
            locationX1 += 65;
        }
    }
}