/**
 * Justin Brooks
 * CS350 Project 6
 *
 * Server side window (player1) of the networked domino game.
 * Moves are processed by sending messages to the client based
 * on mouse events in this window.
 */

import java.awt.event.*;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.*;

public class Server extends JPanel implements MouseListener, KeyListener, MouseMotionListener
{
    private ObjectOutputStream output; // output stream to client
    private ObjectInputStream input; // input stream from client
    private ServerSocket server; // server socket
    private Socket connection; // connection to client

    // Domino game members
    private ArrayList<CDomino> dominoPool;
    private ArrayList<CDomino> player1; // server-side player
    private ArrayList<CDomino> player2; // client-side player

    // Used for moving the domino on mouse event
    private CDomino pieceToBeMoved = null;
    private int oldX;
    private int oldY;

    private Integer moveIndex;

    // Used to initialize game or restart game
    public void init()
    {
        Random random = new Random();
        player1 = new ArrayList<CDomino>();
        player2 = new ArrayList<CDomino>();
        dominoPool = new ArrayList<CDomino>();
        for (int i=0; i<7; i++ ){
            for (int j=i; j<7; j++){
                dominoPool.add(new CDomino(-500,-500,i,j));
            }
        }

        while (dominoPool.size() > 21) {
            int idx = random.nextInt(dominoPool.size());
            player1.add(dominoPool.get(idx));
            dominoPool.remove(idx);
        }

        while (dominoPool.size() > 14) {
            int idx = random.nextInt(dominoPool.size());
            player2.add(dominoPool.get(idx));
            dominoPool.remove(idx);
        }

        for (int i=0; i<dominoPool.size(); i++){
            dominoPool.remove(i);
        }

        int index1 = 0;
        int locationX1 = 85;
        int locationY1 = 475;
        while (index1 < 7) {
            player1.get(index1).setLocation(locationX1, locationY1);
            locationX1 += 65;
            index1++;
        }

        int index2 = 0;
        int locationX2 = 85;
        int locationY2 =  13;
        while (index2 < 7) {
            player2.get(index2).setLocation(locationX2, locationY2);
            player2.get(index2).setDecorated(false);
            locationX2 += 65;
            index2++;
        }
    }

    // State Presentation (Double buffered)
    public void paint( Graphics g )
    {
        // State Presentation, using double buffers
        // create the back buffer
        Image backBuffer=createImage(getSize().width, getSize().height);
        Graphics gBackBuffer=backBuffer.getGraphics();
        // clear the back buffer
        gBackBuffer.setColor(Color.white);
        gBackBuffer.clearRect(0, 0, getSize().width, getSize().height);
        gBackBuffer.setColor(Color.blue);
        gBackBuffer.drawLine(0,105,getWidth(),105);
        gBackBuffer.drawLine(0,getHeight()-105,getWidth(),getHeight()-105);
        // draw the pieces to back buffer
        for (CDomino dom : player1) {
            dom.drawDomino(gBackBuffer, true, false);
        }
        for (CDomino dom : player2) {
            dom.drawDomino(gBackBuffer, false, true);
        }
        // copy from back buffer to front
        g.drawImage(backBuffer, 0, 0, null);
    }

    // MouseListener event handlers
    // If right click -> Rotate 90 degrees clockwise and send message
    public void mouseClicked( MouseEvent e )
    {
        String message = "ROTATE ";
        if (e.isMetaDown()) {
            for (int i=0; i<player1.size(); i++) {
                CDomino piece = player1.get(i);
                if (piece.isInside(e.getX(), e.getY())) {
                    piece.rotate(90);
                    message += i;
                    try {
                        output.writeObject(message);
                    } catch (IOException ex) {
                        System.out.println("MouseClicked message not sent.");
                    }
                    repaint();
                    break;
                }
            }
        }
    }

    // Prepares domino to be moved when clicked and send message
    public void mousePressed( MouseEvent e )
    {
        String message = "SELECTED ";
        // search from top down (i.e. back to front)
        for (int i=player1.size()-1; i>=0; i--) {
            CDomino piece=player1.get(i);
            if (piece.isInside(e.getX(), e.getY())) {
                pieceToBeMoved=piece;
                oldX=e.getX();
                oldY=e.getY();
                message += String.format("%d %d %d", i, oldX, oldY);
                try {
                    output.writeObject(message);
                } catch (IOException ex) {
                    System.out.println("MousePressed message not sent.");
                }
                repaint();
                break;
            }
        }
    }

    // set piece to be moved to null when mouse released
    public void mouseReleased( MouseEvent e )
    {
        pieceToBeMoved=null; // no shape selected
    }

    public void mouseEntered( MouseEvent e )
    {
    }

    public void mouseExited( MouseEvent e )
    {
    }

    public void mouseMoved( MouseEvent e )
    {
    }

    // Move domino with mouse dragged and send message
    public void mouseDragged( MouseEvent e )
    {
        if (e.isMetaDown()) return;	// ignore right button

        String message = "MOVE ";
        if (pieceToBeMoved!=null) {
            int newX = e.getX();
            int newY = e.getY();
            pieceToBeMoved.translate(e.getX()-oldX, e.getY()-oldY);
            oldX=e.getX();
            oldY=e.getY();
            message += String.format("%4d %4d %4d %4d", newX, newY, oldX, oldY);
            try {
                output.writeObject(message);
            } catch (IOException ex) {
                System.out.println("MouseDragged message not sent.");
            }
            repaint();
        }
    }

    // Restart the game when r is pressed and send message
    public void keyPressed(KeyEvent e){
        if (e.getKeyCode() == KeyEvent.VK_R) {
            init();
            String message = "RESTART";
            try {
                output.writeObject(message);
                output.flush();
                sendInitilization();
            } catch ( IOException exe ) {
                System.out.println("Restart message not sent.");
            }
            repaint();
        }
    }

    public void keyReleased(KeyEvent e)
    {
    }

    public void keyTyped(KeyEvent e)
    {
    }


    /*
    *  Networking methods for player1 (server-side) of the two player networked
    *  dominoes game.
    */

    // set up GUI
    public Server()
    {
        init();

        this.setFocusable(true);
        this.addMouseListener(this);
        this.addKeyListener(this);
        this.addMouseMotionListener(this);
    }

    public void runServer()
    {
        try // set up server to receive connections; process connections
        {
            server = new ServerSocket( 12345, 100 ); // create ServerSocket

            while ( true )
            {
                try
                {
                    waitForConnection();
                    getStreams();
                    processConnection();
                }
                catch ( EOFException eofException )
                {
                    System.out.println("Player 2 (Client) has disconnected.");
                }
                finally
                {
                    closeConnection();
                }
            }
        }
        catch ( IOException ioException )
        {
            ioException.printStackTrace();
        }
    }

    // wait for connection to arrive, then display connection info
    private void waitForConnection() throws IOException
    {
        connection = server.accept();
    }

    // get streams to send and receive data
    private void getStreams() throws IOException
    {
        output = new ObjectOutputStream( connection.getOutputStream() );
        sendInitilization();

        input = new ObjectInputStream( connection.getInputStream() );
    }

    private void processConnection() throws IOException
    {
        // Gets messages from the client and manipulates appropriate components.
        while (connection.isConnected()) {
            String message = null;

            try {
                message = (String) input.readObject();
            } catch (ClassNotFoundException ex) {
                System.out.println("Message not read in client.");
            }


            if (message.contains("SELECTED")) {
                String tuple = message.substring(9,message.length());
                String[] oldCoordinates = tuple.split("\\s+");
                moveIndex = Integer.parseInt(oldCoordinates[0]);
                pieceToBeMoved = player2.get(moveIndex);
                pieceToBeMoved.setDecorated(true);
                oldX = Integer.parseInt(oldCoordinates[1]);
                oldY = Integer.parseInt(oldCoordinates[2]);
            }

            if (message.contains("ROTATE")) {
                String tuple = message.substring(7,message.length());
                int rotateIndex = Integer.parseInt(tuple);
                CDomino piece = player2.get(rotateIndex);
                piece.rotate(90);
                repaint();
            }

            if (message.contains("MOVE")) {
                String tuple = message.substring(6,message.length()).trim();
                String[] coordinates = tuple.split("\\s+");
                try {
                    int newX = Integer.parseInt(coordinates[0]);
                    int newY = Integer.parseInt(coordinates[1]);
                    pieceToBeMoved.translate(newX-oldX, oldY-newY);
                    oldX = Integer.parseInt(coordinates[2]);
                    oldY = Integer.parseInt(coordinates[3]);
                } catch (NumberFormatException ex) {
                    System.out.println("Message From Server: BAD FORMAT");
                }
                repaint();
            }
        }
    }

    private void closeConnection()
    {
        try
        {
            output.close();
            input.close();
            connection.close();
            connection = null;
        }
        catch ( IOException ioException )
        {
            ioException.printStackTrace();
        }
    }

    // Sends initial game state to client
    private void sendInitilization()
    {
        try {
            output.writeObject(player1);
            output.writeObject(player2);
        } catch (IOException ex) {
            System.out.println("Init send failed.");
        }
    }
}